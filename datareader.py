from readcol import readcol
import numpy as np
import pdb

c    = 2.998 * 1e10 

glist = np.array(['Chameleon-1','Chameleon-2','Taurus-1','Taurus-2','LupusIII-1','Cepheus-1'])
def groupcheck(group):
    
    if group == '--': return glist

    match = np.where(glist == group)[0]
    if len(match) == 0 : 
        return -1
    else: return 1
    

def gread(group):

##testing line
#    filter = readcol('wave.dat',skipline=1)
   # filter = np.loadtxt('wave.dat',skiprows=1,dtype='S')
   # pdb.set_trace()

    if (group == 'Chameleon-1') | (group == 'Chameleon-2'):
        ##load in the data
        group = 'chall_' + group[-1]
        datafile = group+'.txt'
     #   data = readcol(datafile,skipline=1)
        data = np.loadtxt(datafile,skiprows=1,dtype=float)
        #filter = readcol('chall_wave.txt',skipline=1)
        filter = np.loadtxt('chall_wave.txt',skiprows=1,dtype=str)
        wave_vals= filter[:,1].astype(float)
        obj = np.array(['Chall-'+str(int(oo)) for oo in data[:,0]])
        flux = data[:,1:len(filter)+1]
        #qwe = np.where((flux == 99.999) | (flux == 99.99) | (flux == 999.99) | (flux == 9999.99) | (flux == 9999.999) | (flux ==99999.99)  |  (flux == 999.9) | (flux ==9999) )
        qwe = np.where((np.floor(flux).astype(int) == 99) | (np.floor(flux).astype(int) == 999) | (np.floor(flux).astype(int) == 9999) | (np.floor(flux).astype(int) == 99999) | (np.floor(flux).astype(int) == 999999) )

        #pdb.set_trace()
        flux[qwe[0],qwe[1]] = np.nan
    ### wave_vals = np.array([0.365,0.445,0.551,0.658,0.65628,0.806,0.914,0.900,1.25,1.65,2.2,3.45,3.6,4.5,4.75,5.8,6.7,8.0,12.,14.3,24.,25.,60.,70.,100.,160,1200.,1300.])
        freq_vals = (c/wave_vals)*1e4
    else:
        if (group == 'Taurus-1') | (group == 'Taurus-2'):
            group = 'taurus-' + group[-1]
            pref = 'Tau-'
            
        if (group == 'LupusIII-1'):
            group = 'lupusIII'
            pref = 'Lup-'
        if (group=='Cepheus-1'):
            group ='cepheus'
            pref = 'Cep-'
                      
        datafile = group +'.dat'
       # data = readcol(datafile,skipline=1)
        data = np.loadtxt(datafile,skiprows=1,dtype=float)
        obj = np.array([pref+str(int(oo)) for oo in data[:,0]])
        flux = data[:,1:-2]
        
        
        info = np.array(open(datafile,'r').readline().split()[2:-2])##all the filter names
##        filter = readcol('wave.dat',skipline=1)
        filter = np.loadtxt('wave.dat',skiprows=1,dtype=str)

        wave_vals = np.zeros(len(flux[0]))
        for i in range(len(info)):
           # pdb.set_trace()
            qwe = np.where(filter[:,0] == info[i])[0]
            ##print(i)
            if len(qwe) == 0: 
                print('missing filter!!')
                pdb.set_trace()
                
            ##if i == 15:pdb.set_trace()
            wave_vals[i] = filter[qwe[0],1].astype(float)
        #pdb.set_trace()
        
        
        
        
        
        qwe = np.where((np.floor(flux).astype(int) == 99) | (np.floor(flux).astype(int) == 999) | (np.floor(flux).astype(int) == 9999) | (np.floor(flux).astype(int) == 99999) | (np.floor(flux).astype(int) == 999999) )
       ## qwe = np.where((flux == 99.999) | (flux > 999.98))
        flux[qwe[0],qwe[1]] = np.nan
    ### wave_vals = np.array([0.365,0.445,0.551,0.658,0.65628,0.806,0.914,0.900,1.25,1.65,2.2,3.45,3.6,4.5,4.75,5.8,6.7,8.0,12.,14.3,24.,25.,60.,70.,100.,160,1200.,1300.])
        freq_vals = (c/wave_vals)*1e4
        #pdb.set_trace()
        
        
    return wave_vals,freq_vals,flux,obj